#ifndef MAZE_HPP
#define MAZE_HPP

#include <SFML/Graphics.hpp>
#include <vector>

class Maze : public sf::Drawable {
private:
  enum CellData { Empty,
                  Wall,
                  Dot,
                  SuperDot,
                  Bonus,
                  Error };

  sf::RenderTexture renderTexture;

  sf::Texture &texture;

  sf::Vector2u size;
  std::vector<CellData> data;
  unsigned int dotsCount;

  sf::Vector2i pacwomanPosition;
  std::vector<sf::Vector2i> ghostPositions;

  void draw(sf::RenderTarget &target, sf::RenderStates states) const;

public:
  Maze(sf::Texture &aTexture);
  ~Maze();

  void load_from_file(const std::string &name);
  void pick_object(const sf::Vector2i &position);
  void clear();

  bool is_dot(const sf::Vector2i &position) const;
  bool is_super_dot(const sf::Vector2i &position) const;
  bool is_bonus(const sf::Vector2i &position) const;
  bool is_wall(const sf::Vector2i &position) const;

  const sf::Vector2u &get_size() const;
  const sf::Vector2i &get_pacwoman_position() const;
  const std::vector<sf::Vector2i> &get_ghost_positions() const;
  const unsigned int get_dots_count() const;

  inline std::size_t position_to_index(const sf::Vector2i &position) const;
  inline sf::Vector2i index_to_position(unsigned int index) const;

  inline sf::Vector2i map_pixel_to_cell(const sf::Vector2f &pixelPosition) const;
  inline sf::Vector2f map_cell_to_pixel(const sf::Vector2i &cellPosition) const;
};

std::size_t Maze::position_to_index(const sf::Vector2i &position) const {
  return position.y * size.x + position.x;
}

sf::Vector2i Maze::index_to_position(unsigned int index) const {
  return sf::Vector2i(index % size.x, index / size.y);
}

sf::Vector2i Maze::map_pixel_to_cell(const sf::Vector2f &pixelPosition) const {
  // for positive value only
  // smart people advising to use std::floor
  return sf::Vector2i(static_cast<int>(pixelPosition.x / 32), static_cast<int>(pixelPosition.y / 32));
}

sf::Vector2f Maze::map_cell_to_pixel(const sf::Vector2i &cellPosition) const {
  // for positive value only
  return sf::Vector2f(static_cast<float>(cellPosition.x * 32 + 16), static_cast<float>(cellPosition.y * 32 + 16));
}

#endif // MAZE_HPP
