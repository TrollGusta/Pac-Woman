#ifndef DOT_HPP
#define DOT_HPP

#include <SFML/Graphics.hpp>

sf::CircleShape get_dot();
sf::CircleShape get_super_dot();

#endif // DOT_HPP
