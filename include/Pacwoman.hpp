#ifndef PACWOMAN_HPP
#define PACWOMAN_HPP

#include "Animator.hpp"
#include "Character.hpp"

class Pacwoman : public Character {
private:
  Animator runAnimator;
  Animator dieAnimator;
  Animator *currentAnimator;

  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

public:
  Pacwoman(sf::Texture &texture);
  ~Pacwoman();

  void update(const sf::Time &delta);

  void die();
  void reset();

  bool is_dead() const;

  sf::FloatRect get_collision_box() const override;
};

#endif // PACWOMAN_HPP
