#ifndef GAME_STATE_HPP
#define GAME_STATE_HPP

#include "Ghost.hpp"
#include "Maze.hpp"

#include <SFML/Graphics.hpp>
#include <vector>

class Game;

class GameState {
private:
  Game *game;

public:
  enum State { NoCoin,
               GetReady,
               Playing,
               Won,
               Lost,
               Count };

  GameState(Game *aGame);
  virtual ~GameState();

  virtual void insert_coin() = 0;
  virtual void press_start() = 0;
  virtual void move(const sf::Vector2i &direction) = 0;
  virtual void update(const sf::Time &delta) = 0;
  virtual void draw(sf::RenderWindow &window) = 0;

  Game *const get_game() const;
};

class NoCoinState : public GameState {
private:
  sf::Sprite sprite;

  sf::Time timeBuffer;

  sf::Text text;
  bool displayText;

public:
  NoCoinState(Game *aGame);
  ~NoCoinState();

  void insert_coin();
  void press_start();
  void move(const sf::Vector2i &direction);
  void update(const sf::Time &delta);
  void draw(sf::RenderWindow &window);
};

class PlayingState final : public GameState {
private:
  sf::View camera;

  sf::RenderTexture scene;

  Maze maze;

  Pacwoman *pacwoman;
  std::vector<Ghost *> ghosts;

  unsigned int liveCount;
  sf::Sprite liveSprites[3];

  sf::Text scoreText;
  sf::Text levelText;
  sf::Text dotsText;

  void update_camera(Character *const object);
  void purge_ghosts();

public:
  PlayingState(Game *aGame);
  ~PlayingState();

  void insert_coin();
  void press_start();
  void move(const sf::Vector2i &direction);
  void update(const sf::Time &delta);
  void draw(sf::RenderWindow &window);

  void load_level(unsigned int level);
};


class GetReadyState : public GameState {
private:
  sf::Text text;

  PlayingState *playingState;

  bool waitingPlayer;

public:
  GetReadyState(Game *aGame, PlayingState *playingState);
  ~GetReadyState();

  void insert_coin();
  void press_start();
  void move(const sf::Vector2i &direction);
  void update(const sf::Time &delta);
  void draw(sf::RenderWindow &window);
};

class WonState : public GameState {
private:
  sf::Time timeBuffer;

  sf::Text text;

public:
  WonState(Game *aGame);
  ~WonState();

  void insert_coin();
  void press_start();
  void move(const sf::Vector2i &direction);
  void update(const sf::Time &delta);
  void draw(sf::RenderWindow &window);
};

class LostState : public GameState {
private:
  sf::Time timeBuffer;

  sf::Text scoreText;
  sf::Text text;
  sf::Text textCountDown;

public:
  LostState(Game *aGame);
  ~LostState();

  void insert_coin();
  void press_start();
  void move(const sf::Vector2i &direction);
  void update(const sf::Time &delta);
  void draw(sf::RenderWindow &window);
};

#endif // GAME_STATE_HPP
