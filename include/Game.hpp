#ifndef GAME_HPP
#define GEME_HPP

#include "../include/GameState.hpp"
#include <SFML/Graphics.hpp>
#include <array>

class Game {
private:
  sf::RenderWindow window;

  sf::Time frameTime;

  sf::Font font;
  sf::Texture logo;
  sf::Texture texture;

  GameState *currentState;
  std::array<GameState *, GameState::State::Count> gameStates;
  
  unsigned int currentLevel;
  const unsigned int MAX_MAP;
  const unsigned int MAX_DIFFICULT;

  unsigned int score;

public:
  Game();
  ~Game();

  void run();
  void change_game_state(GameState::State gameState);
  void increase_level();
  void reset_progress();
  void add_points(unsigned int points);

  sf::Font &get_font();
  sf::Texture &get_logo();
  sf::Texture &get_texture();
  unsigned int get_current_level() const;
  const unsigned int get_max_map() const;
  const unsigned int get_max_difficult() const;
  unsigned int get_score() const;
};

#endif // GAME_HPP
