#ifndef GHOST_HPP
#define GHOST_HPP

#include "Pacwoman.hpp"

class Ghost : public Character {
private:
  Pacwoman *pacwoman;

  sf::Vector2i previousPosition;

  Animator strongAnimator;
  Animator weakAnimator;
  Animator *currentAnimator;

  bool isWeak;
  sf::Time weaknessDuration;

  void draw(sf::RenderTarget &target, sf::RenderStates states) const override;

  float get_target_angle() const;

public:
  Ghost(sf::Texture &texture, Pacwoman *aPacwoman);
  ~Ghost();

  void update(const sf::Time &delta);

  void set_weak(const sf::Time &duration);

  bool is_weak() const;

  sf::Vector2i pick_direction();
};

#endif // GHOST_HPP
