#ifndef BONUS_HPP
#define BONUS_HPP

#include <SFML/Graphics.hpp>

class Bonus : public sf::Drawable, public sf::Transformable {
private:
  sf::Sprite sprite;

  void draw(sf::RenderTarget &target, sf::RenderStates states) const;

public:
  enum Fruit { Banana,
               Apple,
               Cherry,
               Count };

  Bonus(sf::Texture &texture);
  ~Bonus();

  void set_fruit(Bonus::Fruit fruit);
};

#endif // BONUS_HPP
