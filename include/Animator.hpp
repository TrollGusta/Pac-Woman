#ifndef ANIMATOR_HPP
#define ANIMATOR_HPP

#include <SFML/Graphics.hpp>
#include <vector>

class Animator {
private:
  std::vector<sf::IntRect> frames;

  bool isPlaying;
  bool isLoop;
  sf::Time duration;

  uint16_t currentFrame;

  sf::Time frameDuration;
  sf::Time timeBuffer;

public:
  Animator();
  ~Animator();

  void add_frame(const sf::IntRect &frame);
  void add_frame(const std::initializer_list<sf::IntRect> &frames);

  void play(const sf::Time &aDuration, bool aLoop);
  bool is_playing() const;

  void set(sf::Sprite &sprite);
  void update(const sf::Time &delta);
};

#endif // ANIMATOR_HPP
