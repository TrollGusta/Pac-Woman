#ifndef CHARACTER_HPP
#define CHARACTER_HPP

#include "Maze.hpp"

#include <SFML/Graphics.hpp>

class Character : public sf::Drawable, public sf::Transformable {
protected:
  sf::Sprite sprite;

  Maze *maze;

  float speed;

  sf::Vector2i nextDirection;

  sf::Vector2i currentDirection;
  sf::Vector2i cellPosition;
  sf::Vector2f offset;

  virtual void draw(sf::RenderTarget &target, sf::RenderStates states) const = 0;

public:
  Character(sf::Texture &texture);
  ~Character();

  virtual void update(const sf::Time &delta);

  inline void set_direction(const sf::Vector2i &direction);
  inline void set_speed(float aSpeed);
  inline void set_maze(Maze *aMaze);

  inline float get_speed() const;
  inline const sf::Vector2i &get_cell_position() const;
  inline const sf::Vector2f &get_offset() const;
  inline const sf::Vector2i &get_direction() const;

  virtual sf::FloatRect get_collision_box() const;
};

void Character::set_direction(const sf::Vector2i &direction) {
  nextDirection = direction;
}

void Character::set_speed(float aSpeed) {
  speed = aSpeed;
}

void Character::set_maze(Maze *aMaze) {
  maze = aMaze;
}

float Character::get_speed() const {
  return speed;
}

const sf::Vector2i &Character::get_cell_position() const {
  return cellPosition;
}

const sf::Vector2f &Character::get_offset() const {
  return offset;
}

const sf::Vector2i &Character::get_direction() const {
  return currentDirection;
}

#endif // CHARACTER_HPP
