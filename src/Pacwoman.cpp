#include "../include/Pacwoman.hpp"

void Pacwoman::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  if (currentAnimator->is_playing()) {
    Character::draw(target, states);
  }
}

Pacwoman::Pacwoman(sf::Texture &texture)
  : Character(texture),
  currentAnimator{ &runAnimator } {
  setOrigin(20, 20);

  runAnimator.add_frame({ sf::IntRect(0, 0, 40, 40),
                         sf::IntRect(0, 40, 40, 40) });
  runAnimator.play(sf::seconds(0.25), true);

  dieAnimator.add_frame({ sf::IntRect(0, 0, 40, 40),
                         sf::IntRect(0, 40, 40, 40),
                         sf::IntRect(0, 80, 40, 40),
                         sf::IntRect(40, 80, 40, 40),
                         sf::IntRect(80, 80, 40, 40),
                         sf::IntRect(120, 80, 40, 40),
                         sf::IntRect(160, 80, 40, 40) });
}

Pacwoman::~Pacwoman() {}

void Pacwoman::update(const sf::Time &delta) {
  if (currentAnimator == &runAnimator) {
    Character::update(delta);
  }
  if (currentAnimator->is_playing()) {
    currentAnimator->update(delta);
    currentAnimator->set(sprite);
  }
}

void Pacwoman::die() {
  nextDirection = sf::Vector2i{ 0, 0 };

  dieAnimator.play(sf::seconds(1), false);
  currentAnimator = &dieAnimator;
}

void Pacwoman::reset() {
  currentAnimator = &runAnimator;
  nextDirection = sf::Vector2i{ 0,0 };
}

bool Pacwoman::is_dead() const {
  return !(currentAnimator->is_playing());
}

sf::FloatRect Pacwoman::get_collision_box() const {
  if (currentAnimator == &dieAnimator) {
    return sf::FloatRect{ 0, 0, 0, 0 };
  } else {
    return Character::get_collision_box();
  }
}