#include "../include/Character.hpp"

#include <cmath>
#include <iostream>

void Character::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  states.transform *= getTransform();
  target.draw(sprite, states);

  sf::RectangleShape collisionBox;
  collisionBox.setFillColor(sf::Color::Transparent);
  collisionBox.setOutlineThickness(1.f);
  collisionBox.setOutlineColor(sf::Color::Red);

  sf::FloatRect bounds = get_collision_box();
  collisionBox.setSize(sf::Vector2f{ bounds.width, bounds.height });
  collisionBox.setOrigin(bounds.width / 2, bounds.height / 2);
  collisionBox.setPosition(getPosition());

  target.draw(collisionBox);
}

Character::Character(sf::Texture &texture)
  : sprite{ texture },
  maze{ nullptr },
  speed{ 100.f },
  nextDirection{ 0, 0 },
  currentDirection{ 0, 0 },
  offset{ 0, 0 } {
}

Character::~Character() {}

void Character::update(const sf::Time &delta) {
  sf::Vector2f pixelPosition = getPosition();
  sf::Vector2f nextPixelPosition = pixelPosition + static_cast<sf::Vector2f>(currentDirection) * speed * delta.asSeconds();
  setPosition(nextPixelPosition);

  cellPosition = maze->map_pixel_to_cell(pixelPosition);

  offset = sf::Vector2f(static_cast<float>(std::fmod(pixelPosition.x, 32) - 16), static_cast<float>(std::fmod(pixelPosition.y, 32) - 16));

  if (maze->is_wall(cellPosition + currentDirection)) {
    if ((currentDirection.x == 1 && offset.x >= 0) ||
        (currentDirection.x == -1 && offset.x <= 0) ||
        (currentDirection.y == 1 && offset.y >= 0) ||
        (currentDirection.y == -1 && offset.y <= 0)) {
      setPosition(maze->map_cell_to_pixel(cellPosition));
    }
  }

  if (nextDirection != currentDirection && !maze->is_wall(cellPosition + nextDirection)) {
    if ((!currentDirection.y && (-2 < offset.x && offset.x < 2)) ||
        (!currentDirection.x && (-2 < offset.y && offset.y < 2))) {
      setPosition(maze->map_cell_to_pixel(cellPosition));
      currentDirection = nextDirection;

      if (currentDirection == sf::Vector2i(1, 0)) {
        setRotation(0);
        setScale(-1, 1);
      } else if (currentDirection == sf::Vector2i(-1, 0)) {
        setRotation(0);
        setScale(1, 1);
      } else if (currentDirection == sf::Vector2i(0, 1)) {
        setRotation(90);
        setScale(-1, 1);
      } else if (currentDirection == sf::Vector2i(0, -1)) {
        setRotation(90);
        setScale(1, 1);
      }
    }
  }
}

sf::FloatRect Character::get_collision_box() const {
  sf::FloatRect bounds{ 6.f, 6.f, 28.f, 28.f };

  return getTransform().transformRect(bounds);
}