#include "../include/Dot.hpp"

sf::CircleShape get_dot() {
  sf::CircleShape dot;
  dot.setRadius(4.f);
  dot.setFillColor(sf::Color::White);
  dot.setOrigin(4.f, 4.f);

  return dot;
}

sf::CircleShape get_super_dot() {
  sf::CircleShape superDot;
  superDot.setRadius(8.f);
  superDot.setFillColor(sf::Color::White);
  superDot.setOrigin(8.f, 8.f);

  return superDot;
}
