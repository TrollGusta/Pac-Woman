#include "../include/Animator.hpp"

Animator::Animator()
    : isPlaying{false},
      isLoop{false},
      duration{sf::Time::Zero},
      currentFrame{0},
      frameDuration{sf::Time::Zero},
      timeBuffer{sf::Time::Zero} {
}

Animator::~Animator() {
}

void Animator::add_frame(const sf::IntRect &frame) {
  frames.push_back(frame);
}

void Animator::add_frame(const std::initializer_list<sf::IntRect> &aFrames) {
  frames = aFrames;
}

void Animator::play(const sf::Time &aDuration, bool aLoop) {
  isPlaying = true;
  duration = aDuration;
  isLoop = aLoop;
  frameDuration = duration / static_cast<float>(frames.size());
}

bool Animator::is_playing() const {
  return isPlaying;
}

void Animator::set(sf::Sprite &sprite) {
  sprite.setTextureRect(frames.at(currentFrame));
}

void Animator::update(const sf::Time &delta) {
  if (!isPlaying) {
    return;
  }

  timeBuffer += delta;

  while (timeBuffer >= frameDuration) {
    ++currentFrame;
    if (currentFrame >= frames.size()) {
      if (!isLoop) {
        isPlaying = false;
      }
      currentFrame = 0;
    }

    timeBuffer -= frameDuration;
  }
}
