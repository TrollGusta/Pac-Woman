#include "../include/Game.hpp"
#include "../include/Path.hpp" // Will be generated by cmake from include/input/Path.hpp.in

#include <exception>
#include <iostream>

Game::Game()
  : window{ sf::VideoMode(480, 500), "Pac-woman" },
  frameTime{ sf::Time::Zero },
  currentState{ nullptr },
  currentLevel{ 0 },
  MAX_MAP{ 3 },
  MAX_DIFFICULT{ 3 },
  score{ 0 } {
  if (!font.loadFromFile(SOURCE_DIR "/assets/font.ttf"))
    throw std::runtime_error("Unable to load font file");

  if (!logo.loadFromFile(SOURCE_DIR "/assets/logo.png"))
    throw std::runtime_error("Unable to load logo file");

  if (!texture.loadFromFile(SOURCE_DIR "/assets/texture.png"))
    throw std::runtime_error("Unable to load texture file");

  gameStates[static_cast<unsigned int>(GameState::NoCoin)] = new NoCoinState(this);
  gameStates[static_cast<unsigned int>(GameState::Playing)] = new PlayingState(this);
  gameStates[static_cast<unsigned int>(GameState::GetReady)] = new GetReadyState(this, 
                                                                                 static_cast<PlayingState *>(gameStates.at(static_cast<unsigned int>
                                                                                                             (GameState::Playing))));
  gameStates[static_cast<unsigned int>(GameState::Won)] = new WonState(this);
  gameStates[static_cast<unsigned int>(GameState::Lost)] = new LostState(this);

  window.setFramerateLimit(60);
}

Game::~Game() {
  for (GameState *gameState : gameStates) {
    delete gameState;
  }
}

void Game::run() {
  change_game_state(GameState::NoCoin);

  sf::Clock timer;

  while (window.isOpen()) {
    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed) {
        window.close();
      }
      if (event.type == sf::Event::KeyPressed) {
        switch (event.key.code) {
        case sf::Keyboard::I:
          currentState->insert_coin();
          break;
        case sf::Keyboard::S:
          currentState->press_start();
          break;
        case sf::Keyboard::Left:
          currentState->move(sf::Vector2i(-1, 0));
          break;
        case sf::Keyboard::Right:
          currentState->move(sf::Vector2i(1, 0));
          break;
        case sf::Keyboard::Up:
          currentState->move(sf::Vector2i(0, -1));
          break;
        case sf::Keyboard::Down:
          currentState->move(sf::Vector2i(0, 1));
          break;
        default:
          break;
        }
      }
    }

    currentState->update(frameTime);

    window.clear(sf::Color::Black);
    currentState->draw(window);
    window.display();

    frameTime = timer.restart();
  }
}

void Game::change_game_state(GameState::State gameState) {
  currentState = gameStates[static_cast<unsigned int>(gameState)];
}

void Game::increase_level() {
  if (currentLevel == MAX_DIFFICULT * MAX_MAP - 1) {
    currentLevel = 0;
  } else {
    ++currentLevel;
  }
}

void Game::reset_progress() {
  currentLevel = 0;
  score = 0;
}

void Game::add_points(unsigned int points) {
  score += points;
}

sf::Font &Game::get_font() {
  return font;
}

sf::Texture &Game::get_logo() {
  return logo;
}

sf::Texture &Game::get_texture() {
  return texture;
}

unsigned int Game::get_current_level() const {
  return currentLevel;
}

const unsigned int Game::get_max_map() const {
  return MAX_MAP;
}

const unsigned int Game::get_max_difficult() const {
  return MAX_DIFFICULT;
}

unsigned int Game::get_score() const {
  return score;
}