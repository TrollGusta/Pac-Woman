#include "../include/Ghost.hpp"

#include <cmath>
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

#include <iostream>

void Ghost::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  Character::draw(target, states);

  sf::RectangleShape lineToTarget;
  lineToTarget.setSize(sf::Vector2f(30, 3));
  lineToTarget.setFillColor(sf::Color::Red);
  lineToTarget.setPosition(getPosition());
  lineToTarget.setRotation(get_target_angle());
  target.draw(lineToTarget);
}

float Ghost::get_target_angle() const {
  sf::Vector2f distance{ pacwoman->getPosition() - getPosition() };
  if (isWeak) {
    return static_cast<float>(std::atan2(-distance.y, -distance.x) * (180 / M_PI));
  } else {
    return static_cast<float>(std::atan2(distance.y, distance.x) * (180 / M_PI));
  }
}

Ghost::Ghost(sf::Texture &texture, Pacwoman *aPacwoman)
  : Character(texture),
  pacwoman{ aPacwoman },
  currentAnimator{ &strongAnimator },
  isWeak{ false },
  weaknessDuration{ sf::Time::Zero } {
  setOrigin(20, 20);

  strongAnimator.add_frame({ sf::IntRect(40, 0, 40, 40),
                            sf::IntRect(40, 40, 40, 40) });
  strongAnimator.play(sf::seconds(0.25f), true);

  weakAnimator.add_frame({ sf::IntRect(80, 0, 40, 40),
                          sf::IntRect(80, 40, 40, 40) });
  weakAnimator.play(sf::seconds(1), true);
}

void Ghost::update(const sf::Time &delta) {
  Character::update(delta);

  sf::Vector2i bestDirection;
  if (previousPosition != cellPosition && getPosition() != pacwoman->getPosition()) {
    set_direction(pick_direction());
    previousPosition = cellPosition;
  }

  if (isWeak) {
    weaknessDuration -= delta;
    if (weaknessDuration <= sf::Time::Zero) {
      isWeak = false;
      currentAnimator = &strongAnimator;
    }
  }

  currentAnimator->update(delta);
  currentAnimator->set(sprite);
}

Ghost::~Ghost() {}

void Ghost::set_weak(const sf::Time &duration) {
  isWeak = true;
  weaknessDuration = duration;
  currentAnimator = &weakAnimator;

  set_direction(-currentDirection);
}

bool Ghost::is_weak() const {
  return isWeak;
}

sf::Vector2i Ghost::pick_direction() {
  static const std::map<float, sf::Vector2i> directions{ {-90.f, sf::Vector2i(0, -1)},
                                                         {0.f, sf::Vector2i(1, 0)},
                                                         {90.f, sf::Vector2i(0, 1)},
                                                         {180.f, sf::Vector2i(-1, 0)} };

  float targetAngle{ get_target_angle() };
  std::map<float, sf::Vector2i> directionProb;
  for (auto &direction : directions) {
    float diff = 180 - std::abs(std::abs(targetAngle - direction.first) - 180);
    directionProb[diff] = direction.second;
  }

  auto best = directionProb.begin();
  while (maze->is_wall(cellPosition + best->second) || best->second == -currentDirection) {
    ++best;
    if (best == directionProb.end()) {
      --best;
      break;
    }
  }

  if (maze->is_wall(cellPosition + best->second)) {
    best->second = -currentDirection;
  }
  
  return best->second;
}
