#include "../include/Bonus.hpp"

void Bonus::draw(sf::RenderTarget &target, sf::RenderStates states) const {
  states.transform *= getTransform();
  target.draw(sprite, states);
}

Bonus::Bonus(sf::Texture &texture)
    : sprite{texture} {
  set_fruit(Banana);
  sprite.setOrigin(15.f, 15.f);
}

Bonus::~Bonus() {
}

void Bonus::set_fruit(Bonus::Fruit fruit) {
  switch (fruit) {
  case Banana:
    sprite.setTextureRect(sf::IntRect(32, 0, 30, 30)); //TODO: change to correct position and size
    break;
  case Apple:
    sprite.setTextureRect(sf::IntRect(32 + 30, 0, 30, 30));
    break;
  case Cherry:
    sprite.setTextureRect(sf::IntRect(32 + 60, 0, 30, 30));
    break;
  default:
    break;
  }
}
