#include "../include/GameState.hpp"
#include "../include/Game.hpp"

#include <algorithm>
#include <iostream>

template <typename T>
void cenet_origin(T &drawable) {
  sf::FloatRect bound = drawable.getLocalBounds();
  drawable.setOrigin(bound.width / 2, bound.height / 2);
}

// Game State
GameState::GameState(Game *aGame)
  : game{ aGame } {}

GameState::~GameState() {}

Game *const GameState::get_game() const {
  return game;
}
//

// No Coin State
NoCoinState::NoCoinState(Game *aGame)
  : GameState(aGame),
  timeBuffer{ sf::Time::Zero },
  displayText{ true } {
  sprite.setTexture(aGame->get_logo());
  sprite.setPosition(20.f, 50.f);

  text.setFont(aGame->get_font());
  text.setString("Insert coin");
  cenet_origin(text);
  text.setPosition(240, 150);
}

NoCoinState::~NoCoinState() {}

void NoCoinState::insert_coin() {
  get_game()->change_game_state(GameState::GetReady);
}

void NoCoinState::press_start() {}

void NoCoinState::move(const sf::Vector2i &direction) {}

void NoCoinState::update(const sf::Time &delta) {
  timeBuffer += delta;
  if (timeBuffer >= sf::seconds(0.5)) {
    displayText = !displayText;
    timeBuffer -= sf::seconds(0.5);
  }
}

void NoCoinState::draw(sf::RenderWindow &window) {
  window.draw(sprite);
  if (displayText) {
    window.draw(text);
  }
}
//

// Get Ready State
GetReadyState::GetReadyState(Game *aGame, PlayingState *playingState)
  : GameState(aGame),
  playingState{ playingState },
  waitingPlayer{ false } {
  text.setFont(aGame->get_font());
  text.setString("Pres 'S' when you are ready...");
  text.setCharacterSize(14);
  cenet_origin(text);
  text.setPosition(240.f, 240.f);
}

GetReadyState::~GetReadyState() {}

void GetReadyState::insert_coin() {}

void GetReadyState::press_start() {
  waitingPlayer = false;
  get_game()->change_game_state(GameState::Playing);
}

void GetReadyState::move(const sf::Vector2i &direction) {}

void GetReadyState::update(const sf::Time &delta) {
  if (!waitingPlayer) {
    playingState->load_level(get_game()->get_current_level());
    playingState->update(delta);
    waitingPlayer = true;
  }
}

void GetReadyState::draw(sf::RenderWindow &window) {
  playingState->draw(window);
  window.draw(text);
}
//

// Playing state
PlayingState::PlayingState(Game *aGame)
  : GameState(aGame),
  maze{ aGame->get_texture() },
  pacwoman{ new Pacwoman{ aGame->get_texture() } },
  liveCount{ 3 } {
  camera.setSize(480, 480);
  scene.create(480, 480);

  scoreText.setFont(get_game()->get_font());
  scoreText.setCharacterSize(10);
  scoreText.setPosition(10, static_cast<float>(scene.getSize().y));
  
  levelText.setFont(get_game()->get_font());
  levelText.setCharacterSize(10);
  levelText.setPosition(160, static_cast<float>(scene.getSize().y));

  dotsText.setFont(get_game()->get_font());
  dotsText.setCharacterSize(10);
  dotsText.setPosition(280, static_cast<float>(scene.getSize().y));

  float positionX{ 415.f };
  for (auto &sprite : liveSprites) {
    sprite.setTexture(get_game()->get_texture());
    sprite.setTextureRect(sf::IntRect(0, 40, 40, 40));
    sprite.setScale(0.5f, 0.5f);
    sprite.setPosition(positionX, static_cast<float>(scene.getSize().y));
    positionX += 20.f;
  }
}

PlayingState::~PlayingState() {
  purge_ghosts();
  delete pacwoman;
}

void PlayingState::insert_coin() {
  // debug option
  pacwoman->die();
  //
}

void PlayingState::press_start() {
  // debug option
  for (auto ghost : ghosts) {
    ghost->set_weak(sf::seconds(3));
  }
  //
}

void PlayingState::move(const sf::Vector2i &direction) {
  pacwoman->set_direction(direction);
}

void PlayingState::update(const sf::Time &delta) {
  static unsigned int attemptScore{ 0 };

  pacwoman->update(delta);

  if (-2 <= pacwoman->get_offset().x && pacwoman->get_offset().x <= 2 &&
      -2 <= pacwoman->get_offset().y && pacwoman->get_offset().y <= 2) {
    if (maze.is_dot(pacwoman->get_cell_position())) {
      attemptScore += 5;
    }

    if (maze.is_super_dot(pacwoman->get_cell_position())) {
      for (auto ghost : ghosts) {
        ghost->set_weak(sf::seconds(5.f));
      }
      attemptScore += 25;
    }

    if (maze.is_bonus(pacwoman->get_cell_position())) {
      attemptScore += 500;
    }

    maze.pick_object(pacwoman->get_cell_position());
  }

  auto ghost = ghosts.begin();
  while (ghost < ghosts.end()) {
    (*ghost)->update(delta);
    if ((*ghost)->get_collision_box().intersects(pacwoman->get_collision_box())) {
      if ((*ghost)->is_weak()) {
        delete (*ghost);
        ghost = ghosts.erase(ghost);

        attemptScore += 100;
      } else {
        pacwoman->die();
        ++ghost;
      }
    } else {
      ++ghost;
    }
  }

  dotsText.setString(std::to_string(maze.get_dots_count()) + " DOTS LEFT");

  scoreText.setString(std::to_string(get_game()->get_score() + attemptScore) + " POINTS");

  if (maze.get_dots_count() <= 0 /* DEBUG OPTION */ || sf::Keyboard::isKeyPressed(sf::Keyboard::N)) {
    liveCount = 3;
    get_game()->add_points(attemptScore);
    attemptScore = 0;
    get_game()->change_game_state(GameState::Won);
  }

  if (pacwoman->is_dead()) {
    attemptScore = 0;

    if (liveCount == 1) {
      liveCount = 3;
      get_game()->change_game_state(GameState::Lost);
    } else {
      --liveCount;
      load_level(get_game()->get_current_level());
    }
  }

  update_camera(pacwoman);
}

void PlayingState::draw(sf::RenderWindow &window) {
  scene.setView(camera);

  scene.clear();

  scene.draw(maze);
  scene.draw(*pacwoman);
  for (auto ghost : ghosts) {
    scene.draw(*ghost);
  }

  scene.display();

  window.draw(sf::Sprite(scene.getTexture()));
  window.draw(scoreText);
  window.draw(levelText);
  window.draw(dotsText);

  for (unsigned int live = 0; live < liveCount; ++live) {
    window.draw(liveSprites[live]);
  }
}

void PlayingState::update_camera(Character *const object) {
  camera.setCenter(object->getPosition());
  if (camera.getCenter().x < camera.getSize().x / 2) {
    camera.setCenter(camera.getSize().x / 2, camera.getCenter().y);
  } else if (camera.getCenter().x > maze.get_size().x * 32 - camera.getSize().x / 2) {
    camera.setCenter(maze.get_size().x * 32 - camera.getSize().x / 2, camera.getCenter().y);
  }
  if (camera.getCenter().y < camera.getSize().y / 2) {
    camera.setCenter(camera.getCenter().x, camera.getSize().y / 2);
  } else if (camera.getCenter().y > maze.get_size().y * 32 - camera.getSize().y / 2) {
    camera.setCenter(camera.getCenter().x, maze.get_size().y * 32 - camera.getSize().y / 2);
  }
}

void PlayingState::load_level(unsigned int level) {
  pacwoman->reset();
  purge_ghosts();
  maze.clear();

  unsigned int difficultLevel{ level / get_game()->get_max_difficult() };
  unsigned int mapLevel{ level % get_game()->get_max_map() };

  switch (mapLevel) {
  case 0:
    maze.load_from_file("small");
    break;
  case 1:
    maze.load_from_file("medium");
    break;
  case 2:
    maze.load_from_file("large");
    break;
  default:
    break;
  }

  levelText.setString("LEVEL: " + std::to_string(difficultLevel + 1)
                      + "-" + std::to_string(mapLevel + 1));

  float speed = 100.f + 50 * difficultLevel;

  pacwoman->set_maze(&maze);
  pacwoman->setPosition(maze.map_cell_to_pixel(maze.get_pacwoman_position()));
  pacwoman->set_speed(speed + 25 / (difficultLevel + 1));

  for (auto &ghostPosition : maze.get_ghost_positions()) {
    Ghost *ghost{ new Ghost{get_game()->get_texture(), pacwoman} };
    ghost->set_maze(&maze);
    ghost->setPosition(maze.map_cell_to_pixel(ghostPosition));
    ghost->set_speed(speed);

    ghosts.push_back(ghost);
  }
}

void PlayingState::purge_ghosts() {
  for (auto ghost : ghosts) {
    delete ghost;
  }
  ghosts.clear();
}
//

// Won State
WonState::WonState(Game *aGame)
  : GameState(aGame),
  timeBuffer{ sf::Time::Zero } {
  text.setFont(aGame->get_font());
  text.setString("You Won!");
  text.setCharacterSize(42);
  cenet_origin(text);
  text.setPosition(240, 120);
}

WonState::~WonState() {}

void WonState::insert_coin() {}

void WonState::press_start() {}

void WonState::move(const sf::Vector2i &direction) {}

void WonState::update(const sf::Time &delta) {
  timeBuffer += delta;
  if (timeBuffer >= sf::seconds(5)) {
    timeBuffer = sf::Time::Zero;
    get_game()->increase_level();
    get_game()->change_game_state(GameState::GetReady);
  }
}

void WonState::draw(sf::RenderWindow &window) {
  window.draw(text);
}
//

// Lost State
LostState::LostState(Game *aGame)
  : GameState(aGame),
  timeBuffer{ sf::Time::Zero } {
  text.setFont(aGame->get_font());
  text.setString("You lost");
  text.setCharacterSize(42);
  cenet_origin(text);
  text.setPosition(240, 120);

  textCountDown.setFont(aGame->get_font());
  textCountDown.setCharacterSize(12);
  textCountDown.setString("Insert a coin to continue 10");
  cenet_origin(textCountDown);
  textCountDown.setPosition(240, 240);

  scoreText.setFont(aGame->get_font());
  scoreText.setCharacterSize(20);
  scoreText.setString("SCORE XXXX");
  cenet_origin(scoreText);
  scoreText.setPosition(240, 300);
}

LostState::~LostState() {}

void LostState::insert_coin() {
  timeBuffer = sf::Time::Zero;
  get_game()->change_game_state(GameState::GetReady);
}

void LostState::press_start() {}

void LostState::move(const sf::Vector2i &direction) {}

void LostState::update(const sf::Time &delta) {
  timeBuffer += delta;
  if (timeBuffer >= sf::seconds(10)) {
    timeBuffer = sf::Time::Zero;
    get_game()->reset_progress();
    get_game()->change_game_state(GameState::NoCoin);
  }

  textCountDown.setString("Insert a coin to continue " + std::to_string(10 - static_cast<int>(timeBuffer.asSeconds())));
  scoreText.setString("SCORE " + std::to_string(get_game()->get_score()));
}

void LostState::draw(sf::RenderWindow &window) {
  window.draw(text);
  window.draw(textCountDown);
  window.draw(scoreText);
}
//
