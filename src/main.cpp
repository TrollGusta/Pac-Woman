#include "../include/Game.hpp"

#include <iostream>

int main() {

  Game pacwoman;
  pacwoman.run();

  return EXIT_SUCCESS;
}
